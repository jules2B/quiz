let indexDesQuestions = 0; 
let score = 0; 

const questions = [{ question: "Vrai ou faux : L'Égypte est le pays le plus peuplé d'Afrique ?", answer: true },
    { question: "Vrai ou faux : La Nouvelle-Zélande fait partie du continent australien ?", answer: false },
    { question: "Vrai ou faux : La Grande Muraille de Chine est visible depuis l'espace ?", answer: false },
    { question: "Vrai ou faux : Le Brésil est le pays le plus peuplé d'Amérique du Sud ?", answer: true },
    { question: "Vrai ou faux : L'Argentine est le plus grand pays d'Amérique du Sud en termes de superficie ?", answer: true },
    { question: "Vrai ou faux : Le Vatican est le pays le plus petit du monde en termes de superficie ?", answer: true }, 
    { question: "Vrai ou faux : L'Antarctique est un continent sans population permanente ?", answer: true },];

const questionElement = document.getElementById("question");
const trueBouton = document.getElementById("true-btn");
const falseBouton = document.getElementById("false-btn");
const scoreElement = document.getElementById("score-valeur");

showQuestion();
function showQuestion() { 
    const { question } = questions[indexDesQuestions];questionElement.textContent = question;}

function checkAnswer(answer){ 
    if (answer === questions[indexDesQuestions].answer) {score++;} indexDesQuestions++;
    indexDesQuestions < questions.length ? showQuestion() : endQuiz();
    actualisationDuScore();}

function actualisationDuScore() {
    scoreElement.textContent = score;}

function endQuiz() {
    alert(`Quiz terminé! Ton score est de ${score}/${questions.length}.`);}

trueBouton.addEventListener("click", () => checkAnswer(true));
falseBouton.addEventListener("click", () => checkAnswer(false));